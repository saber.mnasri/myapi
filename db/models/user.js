const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('user',{
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    })
  
    // Class Method
    // Model1.associate = function (models) {
    //     User.belongsTo(models.Model2)
    // }
    return User
}
