const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require('../config/sequelize')[env];
const db = {};

const sequelize = new Sequelize(config);

fs.readdirSync(path.join(__dirname, 'models'))
  .filter(file => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
  })
  .forEach(file => {
    //console.log(sequelize);
    //const model = sequelize['require'](path.join(__dirname, 'models', file));
    const model = require(path.join(__dirname, 'models', file))(sequelize, Sequelize.DataTypes)
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
