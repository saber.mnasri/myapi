/**
 * @param {fastify} app
 */
async function routes(fastify, options) {

  fastify.get("/flushall", async () => {
    await fastify.cache.getCache().flushAll();
    return { status: "ok" };
  });

  fastify.get("/getstats", async () => {
    //return fastify.cache.client._cache.getStats();
    return await fastify.cache.getCache().getStats();
  });

};

module.exports = routes;