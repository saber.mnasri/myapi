const userController = require('../controllers/user.controller');

async function routes(fastify, options) {

  // fastify.get('/items', async (request, reply) => {
  //   return { message: 'Healthcheck REST API using Fastify' };
  // });

  fastify.post(
    "/users",
    {
      schema: {
        body: {
          type: 'object',
          properties: {
              name: {type: 'string'},
              email: {type: 'string'},
              password: {type: 'string'},
          }
        }
      }
    },
    async function (req, res) {
      return userController.createUser(fastify, req, res);
    }
  );

  // fastify.route({
  //   method: 'POST',
  //   url: '/users',
  //   schema: {
  //     body: {
  //       type: 'object',
  //       properties: {
  //           name: {type: 'string'},
  //           email: {type: 'string'},
  //           password: {type: 'string'},
  //       }
  //     }
  //   },
  //   handler: async function (req, res) {
  //     return userController.createUser(fastify, req, res);
  //   }
  // })

  // fastify.get('/users', 
  //   async (request, reply) => {
  //     return { message: 'Healthcheck REST API using Fastify' };
  //   }
  // );

  fastify.route({
    method: 'GET',
    url: '/users',
    schema: {
      querystring: {
        type: "object",
        properties: {
          page: {
            type: "number",
            default: 1,
          },
          pageLength: {
            type: "number",
            default: 10,
          },
        },
        required: [],
      },
    },
    handler: async function (req, res) {
      return userController.getUsers(fastify, req, res);
    }
  })

}

module.exports = routes;
