async function routes(fastify, options) {

  fastify.get('/healthcheck', async (request, reply) => {
    return { message: 'Healthcheck REST API using Fastify' };
  });

  fastify.get('/', (req, reply) => {
    reply.send({ hello: 'world' });
  });

}

module.exports = routes;
