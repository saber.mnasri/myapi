// const { stringify: csvStringify } = require('csv-stringify')

// async function routes(fastify, options) {

//   fastify.get('/reports', (req, reply) => {

//     const data = [
//       { id: 1,  name: 'test',  description: 'this is a test report',  date: new Date(),  actif: false  },
//       { id: 2,  name: 'test 2',  description: 'this is a test report 2',  date: new Date(),  actif: true  },
//       { id: 3,  name: 'test 3',  description: 'this is a test report 3',  date: new Date(),  actif: false  },
//       { id: 4,  name: 'test 4',  description: 'this is a test report 4',  date: new Date(),  actif: false  },
//       { id: 5,  name: 'test 5',  description: 'this is a test report 5',  date: new Date(),  actif: true  },
//     ];

//     reply.header('Content-Disposition', 'attachment; filename="todo-list.csv"')
//     reply.type('text/csv')

//     const csv = csvStringify({
//       quoted_string: true,
//       header: true,
//       columns: ['id', 'name', 'description', 'date', 'actif'],
//       cast: {
//         boolean: (value) => (value ? 'true' : 'false'),
//         date: (value) => value.toISOString(),
//       },
//     })

//     reply.send(csv);
//   });

// }

// module.exports = routes;


const reporterController = require('../controllers/reporter.controller');

async function routes(fastify, options) {

  fastify.route({
    method: 'GET',
    url: '/reports',
    schema: {
      querystring: {
        type: "object",
        properties: {
          page: {
            type: "number",
            default: 1,
          },
          pageLength: {
            type: "number",
            default: 10,
          },
        },
        required: [],
      },
    },
    handler: async function (req, res) {
      return reporterController.getReports(fastify, req, res);
    }
  })

}

module.exports = routes;

