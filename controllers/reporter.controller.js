const { getCSVReport } = require('../services/reporter.service')

const getReports = async (fastify, req, reply) => {

    try {

        const output = await getCSVReport();

        const date = new Date()
        const fileName = 'actions' + '-' + date.getFullYear() + '-' + ( date.getMonth() + 1 ) + '-' + date.getDate() + '_' + date.getHours() + '-' + date.getMinutes() + '-' + date.getSeconds() + '.csv'

        reply.type('text/csv')
        reply.header('Content-disposition', 'attachment; filename=' + fileName);
        reply.send( output );

        return output;
        
    } catch (error) {
        console.error(error)
        reply.code(500).send(error)
    }
}

module.exports = {
    getReports,
}