const userService = require('../services/user.service');

const getUsers = async (fastify, req, res) => {
    
    const data = req.query;

    const results = await userService.getUsers(fastify,data);

    console.log("results");

    return results;
}

const createUser = async (fastify, req, res) => {
    
    const data = req.body;

    const results = await userService.createUser(fastify,data);

    console.log("results");

    return results;
}

module.exports = {
    createUser,
    getUsers
}