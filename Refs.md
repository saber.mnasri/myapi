## refs:

- https://github.com/jkyberneees/low-http-server/blob/5f07246c2f9b6ee48404042ac65d615af1233b3c/tests/0http-integration-ssl.spec.js
- https://github.com/jeka-kiselyov/fastify-mongoose-api/blob/master/test/auth.test.js
- https://github.com/cryptoticket/rn-version-admin/blob/33b333bfea9771fa1e722d138fb5ea2d52802735/backend/test/routes/v1/users.test.js
- https://github.com/fastify/fastify-basic-auth/blob/master/test/index.test.js#L1322
- https://github.com/knolleary/flowforge/tree/1825806487e463c3469b3eabfbc9c67b96ce9d33


## ssl
https://www.codingdefined.com/2016/03/solved-error-unable-to-verify-first.html
https://copyprogramming.com/howto/error-unable-to-verify-the-first-certificate-in-nodejs
https://www.npmjs.com/package/node_extra_ca_certs_mozilla_bundle

## oracle
https://gist.github.com/rcbop/fdce471b3afebb63e571cf728d9997e1
https://gist.github.com/rcbop/80369f293ee24c0db61039a67c6c5ee3
https://gitlab.com/krishnamanchikalapudi/Jenkinsfile/-/blob/develop/Jenkinsfile?ref_type=heads
https://gitlab.com/y-ok/maven-jenkinsfile/-/blob/master/Jenkinsfile?ref_type=heads
https://gitlab.com/rafaelescrich/exemplo-jenkinsfile/-/blob/master/jenkinsfile?ref_type=heads
