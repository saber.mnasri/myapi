const jwt = reuire("jsonwebtoken");

export const signJWT = (data) => {
  return jwt.sign(data, "test", { expiresIn: "24h" });
};

export const verifyJWT = (token) => {
  const decoded = jwt.verify(token, "test");
  return decoded;
};
