const models = require('./db');
const buildApp = require('./server');

require('dotenv').config();

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

async function main() {

  const server = await buildApp(true);

  models.sequelize.sync().then(function() {
    // Run the server
    server.listen({port, host}, (err, address) => {
      if (err) {
          console.log("[ERROR] An error occurred!");
          server.log.error(err);
          process.exit(1);
      }
      console.log(`server listening on ${address}`);
    });
  })
}

main();
