require('dotenv').config();

module.exports = {
    //redisUrl: process.env.REDIS_URL,
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
    tls: process.env.REDIS_TTL
}