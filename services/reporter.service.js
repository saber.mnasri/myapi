const csvStringify = require('csv-stringify').stringify
const moment = require('moment');

async function asyncCsvStringify(data) {
    return await new Promise((resolve, reject) => {
        try {
            csvStringify(data, (e, output) => {
                if (e) {
                    reject(e);
                } else {
                    resolve(output);
                }
            });
        } catch (e) {
            reject(e);
        }
    });
}

const getCSVReport = async () => {

    try {
        const data = [
            { id: 1,  name: 'test',  description: 'this is a test report',  date: new Date(),  actif: false  },
            { id: 2,  name: 'test 2',  description: 'this is a test report 2',  date: new Date(),  actif: true  },
            { id: 3,  name: 'test 3',  description: 'this is a test report 3',  date: new Date(),  actif: false  },
            { id: 4,  name: 'test 4',  description: 'this is a test report 4',  date: new Date(),  actif: false  },
            { id: 5,  name: 'test 5',  description: 'this is a test report 5',  date: new Date(),  actif: true  },
        ];

        const output = await csvStringify( data, {
            quoted_string: true,
            header: true,
            columns: ['id', 'name', 'description', 'date', 'actif'],
            cast: {
                boolean: (value) => (value ? 'true' : 'false'),
                date: (value) => moment(value).format('YYYY-MM-DD') // value.toISOString(),
            },
        })

        return output;

    } catch (error) {
        console.error(error)
        reply.code(500).send(error)
    }
}

module.exports = {
    getCSVReport,
}