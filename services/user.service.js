const User = require("../db")['user'];
const logger = require('../server/logger')
const LIST_PREFIX = "user_list";

/**
 * POST a user
 * @param {*} fastify 
 * @param {*} data 
 * @returns 
 */
const createUser = async (fastify,data) =>{
  const { name, email, password } = data;

  //flash user cache
  fastify.flushKey(LIST_PREFIX);

  //create user
  const results = await User.create({
    name,
    email,
    password,
  });

  console.log("results",results.dataValues);
  return results.dataValues;
}

/**
 * GET users
 * @param {*} fastify 
 * @param {*} data 
 * @returns 
 */
const getUsers = async (fastify,data) =>{

    const { page, pageLength } = data;
    const startIndex = (page - 1) * pageLength;
    const endIndex = page * pageLength;
    const results = {};

    let cacheKey = `${LIST_PREFIX}_${page}_${pageLength}`;
    logger.info(cacheKey);

    const cacheResult = await fastify.cache.get(cacheKey);
    if (cacheResult?.item) {
      return cacheResult.item;
    }

    if (endIndex < (await User.count())) {
      results.next = {
        page: page + 1,
        pageLength,
      };
    }

    if (startIndex > 0) {
      results.previous = {
        page: page - 1,
        pageLength,
      };
    }


    results.totalPage = Math.ceil(
      (await User.count()) / pageLength
    );

    results.data = await User.findAll({
      // where: {
      //     id: [46128, 2865, 49569,  1488,   45600,   61991,  1418,  61919,   53326,   61680]
      // }, 
      // Add order conditions here....
      order: [
          ['id', 'DESC'],
      ],
      // attributes: ['id', 'logo_version', 'logo_content_type', 'name', 'updated_at'],
      limit: pageLength, 
      offset: startIndex,
    });

    await fastify.cache.set(cacheKey, results, 1000 * 60 * 60);

    logger.info(results);
    console.log("results",results);

    return results;
}

module.exports = {
  createUser,
  getUsers
}

