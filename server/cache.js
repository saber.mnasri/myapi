const IORedis = require("ioredis");
const AbstractCache = require("abstract-cache");
const fastifyRedis = require("@fastify/redis");
const fastifyCaching = require("@fastify/caching");
const config = require('../config/redis');
const cacheDecorator = require("./cacheDecorator");
// const logger = require('../config/winston');
const logger = require('../server/logger')
const SEGMENT = "cache";


async function registerCacheRedis(fastify) {

  if (config.host === undefined || config.host?.length === 0) return false;
  
  const redis = new IORedis({
    host: config.host,
    port: config.port,
    ttl: config.ttl,
    enableReadyCheck: true,
  });

  try {
    await new Promise((resolve, reject) => {
      redis.on('connect', () => {
        logger.info('IO Redis is connected');
      });
      redis.on("ready", () => {
        const abcache = AbstractCache({
          useAwait: true,
          driver: {
            name: "abstract-cache-redis",// must be installed via `npm i`
            options: {
              client: redis,
              segment: SEGMENT,
            },
          },
        });
        fastify.register(fastifyRedis, { client: redis });
        fastify.register(fastifyCaching, { cache: abcache });
        cacheDecorator(fastify, SEGMENT);
        logger.info('IO Redis is ready');
        resolve();
      });
      redis.on("error", (error) => {
        logger.error('IO Redis error:',error);
        reject
      });
    });
    return true;
  } catch (error) {
    console.log(error)
    return false;
  }
}

function registerCacheMemory(fastify) {
  const abcache = AbstractCache({
    useAwait: true,
    driver: {
      options: {
        segment: SEGMENT,
      },
    },
  });
  fastify.register(fastifyCaching, {
    cache: abcache,
  });
  cacheDecorator(fastify, SEGMENT);
}


module.exports = async function (fastify) {
  if (await registerCacheRedis(fastify)) return;
  registerCacheMemory(fastify);
};
