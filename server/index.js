const autoload = require('@fastify/autoload')
const logger = require('../config/winston');
const path = require('path');
const basicAuth = require('@fastify/basic-auth');
const fastify = require('fastify');
const fastifySwagger = require('@fastify/swagger');
const fastifySwaggerUi = require('@fastify/swagger-ui');
const buildCache = require("./cache");

const env = process.env.NODE_ENV || 'development';

async function buildApp(loggerStatus) {

    const app = fastify({
        trustProxy: true,
        ignoreTrailingSlash: true,
        disableRequestLogging: true,
        logger: env === "test" ? undefined : logger,
    });
    
    app.setErrorHandler(async (error) => {
        if (error.statusCode) {
            throw error;
        }
    
        app.log.error(error);
        throw app.httpErrors.internalServerError();
    });

    // app.get('/', (req, reply) => {
    //     reply.send({ hello: 'world' });
    // });

    //fastify.register(basicAuth, { validate, authenticate })

    await buildCache(app);

    app.register(fastifySwagger, {
        mode: 'static',
        specification: {
          path: `${require('path').resolve(__dirname, '../openapi/index.yaml')}`,
        },
    });

    app.register(fastifySwaggerUi, {
        routePrefix: '/docs',
        uiConfig: {
            docExpansion: 'none', // expand/not all the documentations none|list|full
            deepLinking: true
        },
        uiHooks: {
            onRequest: function(request, reply, next) {
                next()
            },
            preHandler: function(request, reply, next) {
                next()
            }
        },
        staticCSP: false,
        transformStaticCSP: (header) => header,
        exposeRoute: true
    })

    // This loads all plugins defined in /routes
    app.register(autoload, {
        dir: path.join(__dirname, "../routes"),
        options: Object.assign({ prefix: '/api' }, {})
    });

    return app;

}

module.exports = buildApp;
