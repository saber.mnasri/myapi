DIAPO 1:

Node.js : un environnement d’exécution JavaScript

Node.js n’est pas un langage de programmation. 
Ce n’est pas non plus un framework JavaScript. 
Node.js est un environnement d’exécution JavaScript.

Node représente un environnement d’exécution (runtime), 
un ensemble d’API JavaScript ainsi qu’une machine virtuelle (VM) JavaScript performante (parseur, interpréteur et compilateur) 
capable d’accéder à des ressources système telles que des fichiers (filesystem) ou des connexions réseau (sockets).

npm est une dénomination qui abrite plusieurs concepts : 
- un outil en ligne de commandes, 
- un registre de modules ainsi qu’une entreprise privée à but lucratif (npm, Inc.).


DIAPO 2:
============
1 / installer NODEJS
Il faut d'abord commencer par installer NODES depuis le site nodejs.org
Le matketplace contient déjà une version node que vous pouvez l'installer

Vérifier l’installation de Node depuis un terminal (shell):
node -v

L'outl npm sinstalle aussi avec.
npm -v

puis choisir un IDE pour travailler avec node.
le plus celebre visual studio code


la mise  à our de node peux se faire simplement en installt une nouvelle version plus recente


Les modules de base étendent le champ d’action de Node.

6.1. console : déboguer rapidement des variables
6.2. path : manipuler des chemins de fichiers
6.3. url : manipuler des URL
6.4. fs : manipuler le système de fichiers
6.5. events : programmer des événements
6.6. util : transformer des fonctions de rappel en promesses
6.7. http : créer et interroger des ressources via le protocole HTTP
6.8. os : en savoir plus sur les capacités de l’ordinateur
6.9. child_process : appeler un exécutable système
6.10. process : en savoir plus sur le processus en cours
6.11. stream : manipuler des flux de données
6.12. D’autres modules pour aller plus loin


DIAPO 3:

const fs = require('fs');             
fs.readdir('.', (error, files) => {
  console.log(files);                 
});

On charge les fonctions et attributs du module fs dans la variable du même nom (on pourrait l’appeler autrement).

L’appel à la fonction fs.readdir() passe un objet d’erreur ainsi que la liste des fichiers et répertoires contenus dans le chemin indiqué.

Affiche un tableau contenant les noms de fichiers et de répertoires présents dans le dossier courant.


creaer nos propres modules:
Les modules de base nous fournissent de nombreuses fonctionnalités. 
Nous pouvons réutiliser le même mécanisme pour organiser notre code dans plusieurs fichiers.


fichier: mymodule.js:
const hello = 'hello world';
module.exports = hello


fichier: main.js:
const hello = require('./mymodule');
console.log(hello);


DIAPO 4:

Passons à créer un projet avec NODE/NPM:

Le mot npm correspond à trois concepts différents que nous aborderons tout au long de ce chapitre :
•	l'exécutable npm – un programme écrit en ECMAScript ;
•	le registre npm – une plate-forme de distribution de modules ;
•	un module npm – en général installé depuis le registre et utilisable avec les fonctions require() et import.


1/ Créer un fichier package.json:
Le fichier package.json est la clé de voûte servant à reproduire l’installation du projet et créer un outillage autonome. 
La commande npm init génère un tel fichier. L’utilisation de l’option --yes va plus vite car elle nous évite de répondre aux questions :
il sera créé avec des valeurs par défaut:

npm init --yes


2/ Installer des modules npm:
Les fonctions require() et import chargent nos propres modules mais aussi les modules de base, installés avec Node. 
Les modules npm sont complémentaires et téléchargeables à l’aide de l’exécutable npm.

Depuis le registre npm

Le module est installé et prêt à être inclus dans un script. 
Nous constatons aussi que le champ dependencies est apparu dans le fichier package.json

package.json
{
  ...
  "dependencies": {
    "fastify": "^1.3.1"
  }
}


Désinstaller un module:
npm uninstall fastify

Spécifier une version:
 npm install lodash
 
 
 npm install lodash@3.0.0
 
 
 npm view lodash versions
